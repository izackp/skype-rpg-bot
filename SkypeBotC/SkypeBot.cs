﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using SKYPE4COMLib;

namespace SkypeBotC {
    public partial class SkypeBot : Form {

        [DllImport("kernel32")]
        private static extern int AllocConsole();
        [DllImport("kernel32")]
        private static extern int FreeConsole();

        public Boolean AcceptCalls = false;
        public Boolean AcceptFriends = true;
        public String BotName = "RPG Skype Bot";
        public String Trigger = "!";

        readonly Skype _skype = new Skype();
        String _msgSender = String.Empty;
        readonly String _newLine = Environment.NewLine;

        public SkypeBot() {
            InitializeComponent();
        }

        ~SkypeBot()
        {
            FreeConsole();
        }

        private void FormLoad(object sender, EventArgs e) {

            AllocConsole();
            _skype.MessageStatus += GetChatMessage;
            _skype.CallStatus += GetCallRequest;
            _skype.UserAuthorizationRequestReceived += GetFriendRequest;
            _skype.Attach(9);

            LoadConsole();
            HideSkypeWindows();
        }

        //BEGINNING CONSOLE PRINTING
        private void LoadConsole() {
            Console.Title = BotName + @" <attached to: " + _skype.CurrentUserHandle + @">";
            WriteLine("SkypeBot loaded!", ConsoleColor.Cyan);
            WriteLine();
            WriteLine(" - attached to: " + _skype.CurrentUserHandle, ConsoleColor.Green);
            WriteLine(" - accepting calls: " + AcceptCalls.ToString().ToLower(), ConsoleColor.Cyan);
            WriteLine(" - accepting friends: " + AcceptFriends.ToString().ToLower(), ConsoleColor.Cyan);
            WriteLine();
        }

        private void GetCallRequest(Call call, TCallStatus status)
        {
            if (AcceptCalls) return;

            if (status != TCallStatus.clsRinging) return;

            _skype.SendMessage(call.PartnerHandle, BotName + " does not accept calls.");

            //TODO: why is in try/catch ?
            try { call.Finish(); } catch
            { }
        }

        private void GetFriendRequest(User user) {

            if (!AcceptFriends) return;

            user.IsAuthorized = true;

            WriteLine("[" + DateTime.Now.ToShortTimeString() + "] friend request from \"" + user.Handle + "\"");
        }

        private void GetChatMessage(ChatMessage msg, TChatMessageStatus status)
        {
            if (status != TChatMessageStatus.cmsReceived) return;

            if (msg.Body.IndexOf(Trigger, StringComparison.Ordinal) != 0) return;

            _msgSender = msg.Sender.Handle;

            var command = msg.Body.Remove(0, Trigger.Length);

            _skype.SendMessage(_msgSender, processCommand(command));

            SendSkypeCommand("SET CHATMESSAGE " + msg.Id + " SEEN");
        }

        void SendSkypeCommand(string cmdToSend)
        {
            var cmdSkype = new Command {Blocking = true, Timeout = 2000, Command = cmdToSend};
            _skype.SendCommand(cmdSkype);
        }

        void HideSkypeWindows()
        {
            SendSkypeCommand("SET SILENT_MODE ON");
            SendSkypeCommand("SET WINDOWSTATE HIDDEN");
        }

        private String processCommand(String command) {

            var cmd = command.Split(' ')[0];

            var arg = cmd.Replace(command + " ", String.Empty);

            WriteLine("[" + DateTime.Now.ToLongTimeString() + "] command \"" + cmd + "\" from \"" + _msgSender + "\"", ConsoleColor.Cyan);
            
            string msg;
            switch(cmd) {
                case "commands":
                    msg = String.Empty;
                    msg += "-----USER COMMANDS-----" + _newLine;
                    msg += Trigger + "commands" + _newLine;
                    msg += Trigger + "info" + _newLine;
                    break;
                case "info":
                    msg = String.Empty;
                    msg += BotName + _newLine;
                    msg += "Coded by Recuperare";
                    break;
                default:
                    msg = string.Empty;
                    msg += cmd + " is not a known command!" + _newLine;
                    msg += "Type " + Trigger + "commands for a list of available commands";
                    break;
            }

            return msg;
        }

        private static void WriteLine(string msg = "", ConsoleColor color = ConsoleColor.Gray) {
            Console.ForegroundColor = color;
            Console.WriteLine(msg);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
